from django.test import TestCase
from django.urls import reverse, resolve


class TestClientsURL(TestCase):

    def test_clients_list_reverse(self):
        """clients_list reverse must be /clients/"""
        url = reverse('clients_list')
        expected_response = '/clients/'

        self.assertEqual(url, expected_response)

    def test_clients_detail_reverse(self):
        """clients_list reverse must be /clients/"""
        url = reverse('clients_detail', kwargs={'pk': '1'})
        expected_response = '/clients/1/'

        self.assertEqual(url, expected_response)
