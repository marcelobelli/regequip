from django.test import TestCase

from model_mommy import mommy

from regEquip.clients.models import Client

class TestClientsModel(TestCase):

    def test__str__(self):
        """The __str__ method must return 'document - name' of the client"""
        name = 'Client Test'
        document = '1234567890'
        client = mommy.make(Client, name=name, document=document)

        expected_response = '{0} - {1}'.format(document, name)

        self.assertEqual(client.__str__(), expected_response)
