# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-06-11 18:13
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Client',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Nome')),
                ('document', models.CharField(max_length=14, verbose_name='CPF/CNPJ')),
                ('address', models.CharField(max_length=100, verbose_name='Endereço')),
                ('zipcode', models.CharField(max_length=8, verbose_name='CEP')),
                ('city', models.CharField(max_length=20, verbose_name='Cidade')),
                ('state', models.CharField(max_length=2, verbose_name='Estado')),
                ('phone', models.CharField(max_length=11, verbose_name='Telefone')),
                ('email', models.EmailField(max_length=254, verbose_name='E-mail')),
            ],
        ),
    ]
