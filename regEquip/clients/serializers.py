from rest_framework import serializers

from regEquip.clients.models import Client


class ClientSerializer(serializers.ModelSerializer):

    class Meta:
        model = Client

        fields = (
            'id',
            'name',
            'document',
            'address',
            'zipcode',
            'city',
            'state',
            'phone',
            'email',
        )
