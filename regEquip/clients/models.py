from django.db import models


class Client(models.Model):
    name = models.CharField('Nome', max_length=100)
    document = models.CharField('CPF/CNPJ', max_length=14)
    address = models.CharField('Endereço', max_length=100)
    zipcode = models.CharField('CEP', max_length=8)
    city = models.CharField('Cidade', max_length=20)
    state = models.CharField('Estado', max_length=2)
    phone = models.CharField('Telefone', max_length=11)
    email = models.EmailField('E-mail')

    def __str__(self):
        return '{0} - {1}'.format(self.document, self.name)
